# urbanMicroclimateFoam

An open-source solver for coupled physical processes modeling urban microclimate based on OpenFOAM.

> **⚠️ Information**
>
> The repository for the solver is moved to GitHub. Future updates will be at: 
>
> https://github.com/OpenFOAM-BuildingPhysics/urbanMicroclimateFoam
>
> **⚠️ Information**

| Daily variation of surface temperature in a street canyon | Daily variation of air temperature and wind speed at pedestrian height in Münsterhof, Zürich. |
|:---:|:---:|
| <img src="https://gitlab.ethz.ch/openfoam-cbp/solvers/urbanmicroclimatefoam/-/wikis/uploads/9fb2efac6b6827fa7604c3f58960093f/img1_out.gif"  width="75%"> | <img src="https://gitlab.ethz.ch/openfoam-cbp/solvers/urbanmicroclimatefoam/-/wikis/uploads/95d6a8f84991e20b1223c307ca814fc9/img2b_out.gif"  width="45%"> &nbsp; <img src="https://gitlab.ethz.ch/openfoam-cbp/solvers/urbanmicroclimatefoam/-/wikis/uploads/876fcd8fb7d6b18077f9ddeab44db013/img2c_out.gif"  width="45%"> |

More information at the Chair of Building Physics: https://carmeliet.ethz.ch
